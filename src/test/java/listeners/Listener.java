package listeners;

import main.Main;

import io.qameta.allure.Attachment;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.internal.IResultListener2;
import org.testng.log4testng.Logger;

import java.text.SimpleDateFormat;
import java.util.Date;

public
class Listener
        implements IResultListener2
    {
        private
        final static Logger logger = Logger.getLogger(Listener.class);
        private Date date = new Date();
        private SimpleDateFormat dateFormat = new SimpleDateFormat("E yyyy-MM-dd ' ' HH.mm.ss");


        @Override
        public
        void beforeConfiguration(ITestResult iTestResult)
        {
        }


        @Override
        public
        void onConfigurationSuccess(ITestResult iTestResult)
        {
        }


        @Override
        public
        void onConfigurationFailure(ITestResult iTestResult)
        {
        }


        @Override
        public
        void onConfigurationSkip(ITestResult iTestResult)
        {
        }


        @Override
        public
        void onTestStart(ITestResult iTestResult)
        {
        }


        @Override
        public
        void onTestSuccess(ITestResult iTestResult)
        {
            Reporter.log("Test method succeed " + iTestResult.getName());
        }


        @Override
        public
        void onTestFailure(ITestResult iTestResult)
        {
            logger.error("TEST FAIL " +iTestResult.getName());
            makeScreenshot("Negative " + dateFormat.format(date), iTestResult);
        }


        @Override
        public
        void onTestSkipped(ITestResult iTestResult)
        {
            logger.error("TEST HAS BEING SKIPPED " +iTestResult.getName());
            makeScreenshot("Skipped " + dateFormat.format(date), iTestResult);
        }


        @Override
        public
        void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult)
        {
        }


        @Override
        public
        void onStart(ITestContext iTestContext)
        {
            Reporter.log("Test method is about to start " + iTestContext.getName());
        }


        @Override
        public
        void onFinish(ITestContext iTestContext)
        {
            Reporter.log("Completed executing test " + iTestContext.getName());
        }


        @Attachment(value = "{name}", type = "image/png")
        private
        byte[] makeScreenshot(String name, ITestResult iTestResult_)
        {
            Object currentClass = iTestResult_.getInstance();
            WebDriver driver = ((Main) currentClass).getDriver();
            return ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
        }
    }